﻿using System;

namespace Task6_1
{
    internal class Program
    {
        public static void Main(string[] args)
        {int A = 7, B = 12, sum = 0;
            for (int i = A+1; i < B; i++)
            {
                if (i % 2 != 0)
                {
                    Console.WriteLine(i +" is odd");
                }

                sum += i;
            }

            Console.WriteLine(sum);
        }
    }
}