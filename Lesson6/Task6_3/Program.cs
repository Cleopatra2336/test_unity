﻿using System;

namespace Task6_3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int N = Convert.ToInt32(Console.ReadLine()), variable_marsh = 0;
            if (N > 0) variable_marsh = 1;
            do
            {
                variable_marsh *= N--;
            } while (N > 0);

            Console.WriteLine(variable_marsh);
        }
    }
}