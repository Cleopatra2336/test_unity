﻿using System;

namespace Task6_2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int x = Convert.ToInt32(Console.ReadLine()), y = Convert.ToInt32(Console.ReadLine()), z = 1;
            int step = y;
            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Console.Write("*");
                }

                Console.WriteLine();
            }

            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < i + 1; j++)
                {
                    Console.Write("*");
                }

                Console.WriteLine();
            }


            for (int i = 0; i < x; i++)
            {
                for (int j = 0; j < y; j++)
                {
                    Console.Write(" ");
                }

                for (int k = 0; k < z; k++)
                {
                    Console.Write("*");
                }

                y -= 1;
                z += 2;
                Console.WriteLine();
            }

            for (int i = 0; i < (x + x + 1); i++)
            {
                if (i < x)
                {
                    for (int j = 0; j < step; j++)
                    {
                        Console.Write(" ");
                    }

                    for (int k = 0; k < z; k++)
                    {
                        Console.Write("*");
                    }

                    step -= 1;
                    z += 2;
                    Console.WriteLine();
                }
                else
                {
                    for (int j = 0; j < step; j++)
                    {
                        Console.Write(" ");
                    }

                    for (int k = 0; k < z; k++)
                    {
                        Console.Write("*");
                    }

                    step += 1;
                    z -= 2;
                    Console.WriteLine();
                }
            }
        }
    }
}