using System;

namespace Task7_1
{
    public class calc
    {
        
        public static int Add(int operand1, int operand2)
        {
            return operand1 + operand2;
        }

        public static int Sub(int operand1, int operand2)
        {
            return operand1 - operand2;
        }

        public static int Mul(int operand1, int operand2)
        {
            return operand1 * operand2;
        }

        public static int Div(int operand1, int operand2)
        {
            if (operand2 != 0)
            {
                return operand1 / operand2;
            }
            else
            {
                Console.WriteLine("operand2 is null");
                return 0;
            }
        }
    }
}