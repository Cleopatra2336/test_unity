﻿using System;

namespace Task7_1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("write 1 operand");
            int operand1 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("write 2 operand");
            int operand2 = Convert.ToInt32(Console.ReadLine());

            Console.WriteLine("enter sign: + - * /");
            string sing = Console.ReadLine();

            switch (sing)
            {
                case "+":
                    Console.WriteLine("Сложение {0}", calc.Add(operand1, operand2));
                    break;
                case "-":
                    Console.WriteLine("Вычитание {0}", calc.Sub(operand1, operand2));
                    break;
                case "*":
                    Console.WriteLine("Умножение {0}", calc.Mul(operand1, operand2));
                    break;
                case "/":
                    Console.WriteLine("Деление {0}", calc.Div(operand1, operand2));
                    break;
                default:
                    Console.WriteLine("Вы ввели неверный знак");
                    break;
            }
        }
    }
}