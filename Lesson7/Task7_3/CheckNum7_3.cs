using System;

namespace Task7_3
{
    public class CheckNum7_3
    {
        public static void Otric(int operand)
        {
            operand = operand >> 31;

            if (operand == -1)
            {
                Console.WriteLine("the number is negative");
            }
            else
            {
                Console.WriteLine("the number is positive");
            }
        }

        public static void Remainder(int operand)
        {
            if ((operand % 2) == 0 && (operand % 5) == 0 && (operand % 3) == 0 && (operand % 6) == 0 &&
                (operand % 9) == 0)
            {
                Console.WriteLine("number to share without residue on 2, 5, 3, 6, 9");
            }
            else
            {
                Console.WriteLine("number is not share without residue on 2, 5, 3, 6, 9");
            }
        }

        public static void Simple(int operand)
        {
            int divider; 
            int remainder;
            divider = 2;

            do
            {
                remainder = operand % divider;

                if (remainder != 0)
                    divider++;
            } while (remainder != 0);

            if (divider == operand)
            {
                Console.WriteLine(operand + "- prime number");
            }
            else
            {
                Console.WriteLine(operand + "- not prime number");
            }
        }
    }
}