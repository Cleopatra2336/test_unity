﻿using System;

namespace Task7_3
{
    internal class Program
    {
        public static void Main()
        { 
            Console.Write("Write num to check: ");
            int operand = Convert.ToInt32(Console.ReadLine());

            CheckNum7_3.Simple(operand);
            CheckNum7_3.Otric(operand);
            CheckNum7_3.Remainder(operand);
        }
    }
}