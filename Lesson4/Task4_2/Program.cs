﻿using System;

namespace Task4_2
{
    internal class Program
    {
        public static void Main()
        {
            int num = int.Parse(Console.ReadLine());
            if (0 <= num && num <= 14)
            {
                Console.Write("this value is in range 0-14");
            }
            else if (15 <= num && num <= 35)
            {
                Console.Write("this value is in range 15-35");
            }
            else if (36 <= num && num <= 50)
            {
                Console.Write("this value is in range 36-50");
            }
            else if (51 <= num && num <= 100)
            {
                Console.Write("this value is in range 51-100");
            }
            else
            {
                Console.Write("your value is not lies in the range 0-100");
            }
        }
    }
}