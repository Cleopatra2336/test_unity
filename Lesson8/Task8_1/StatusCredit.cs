using System;

namespace Task8_1
{
    public class StatusCredit
    {
        private int sum_deb = 700;

        public void chek_status(int sum)
        {
            if ((sum_deb - sum) > 0)
            {
                Console.WriteLine("debt equals " + (sum_deb - sum));
            }
            else if (sum_deb - sum < 0)
            {
                Console.WriteLine("overpayment is " + Math.Abs(sum_deb - sum));
            }
            else Console.WriteLine("Debt is clear");
        }
    }
}