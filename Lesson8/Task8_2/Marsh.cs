using System;

namespace Task8_2
{
    public class Marsh
    {
        static private int factorial = 1;

        static public void check_marsh(int N)
        {
            for (int i = 2; i <= N; i++)
            {
                factorial = factorial * i;
            }

            Console.WriteLine("number of possible routes = " + factorial);
        }
    }
}