﻿using System;

namespace Task9_3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            Console.WriteLine("Enter count elem array ");
            int N = Convert.ToInt32(Console.ReadLine());
            Array9_3 array9_3 = new Array9_3(N);
            
            Console.Write("\nEnter value");
            int value = Convert.ToInt32(Console.ReadLine());
            int[] res = array9_3.ArrayBuff(Array9_3.Array_create(),value);
            for (int i=0; i<res.Length;i++)
            {
                Console.Write(res[i] + " ");
            }
        }
    }
}