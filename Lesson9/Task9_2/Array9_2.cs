using System;

namespace Task9_2
{
    public class Array9_2
    {
        static private int countArr;

        public Array9_2(int N)
        {
            countArr = N;
        }

        public static int[] Array_create()
        {
            Random rand = new Random();
            int[] a = new int[countArr];
            for (int i = 0; i < countArr; i++)
            {
                a[i] = rand.Next(1, 50);
                Console.Write(a[i] + " ");
            }

            return a;
        }

        public int[] MyReverse(int[] array)
        {
            Array.Reverse(array);
            for (int i = 0; i < array.Length; i++) Console.Write(array[i]);
            return array;
        }

        public int[] SubArray(int[] array, int index, int count)
        {
            int k = 0;
            int[] subArr = new int[0];

            for (int i = index; k < count; k++)
            {
                if (array[i] != null)
                {

                    subArr[k] = array[i];
                    
                }
                else
                {
                    subArr[k] = 1;
                }
                Console.Write(subArr[k]);
                k++;
            }
            
            
            
        }
    }
}