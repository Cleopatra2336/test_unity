using System;
using System.Linq;

namespace Task9_1
{
    public class Array9_1
    {
        static private int countArr;

        public Array9_1(int N)
        {
            countArr = N;
        }

        public int[] Array_create()
        {
            Random rand = new Random();
            int[] a = new int[countArr];
            for (int i = 0; i < countArr; i++)
            {
                a[i] = rand.Next(1, 50);
                Console.Write(a[i] + " ");
            }

            return a;
        }

        public void Operation(int[] operation)
        {
            int sum = 0,
                min = operation.Min(),
                max = operation.Max();
            for (int i = 1; i < operation.Length; i++)
            {
                sum = sum + operation[i];
            }

            Console.WriteLine("\n Sum all elem " + sum + " Min elem " + min +
                              " Max elem " + max + " Average value all elem " + sum / operation.Length);
            for (int i = 0; i < operation.Length; i++)
            {
                if (operation[i] % 2 != 0)
                {
                    Console.Write(operation[i] + " ");
                }
            }
        }
    }
}