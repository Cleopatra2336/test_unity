﻿using System;

namespace Task4
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            double height_cylinder = 12.0, radius_cylinder = 6.0;
            double volume_cylinder, area_cylinder;
            volume_cylinder = Math.PI * Math.Pow(radius_cylinder, 2) * height_cylinder;
            area_cylinder = 2 * Math.PI * radius_cylinder * (radius_cylinder + height_cylinder);
            Console.WriteLine("V={0}, S={1}", Convert.ToInt32(volume_cylinder), Convert.ToInt32(area_cylinder));
        }
    }
}