﻿using System;

namespace Task1
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int x = 10, y = 12, z = 3;
            Console.WriteLine(x + y - x++ * z);
            Console.WriteLine(--x - y * 5);
            Console.WriteLine(y / (x + 5 % z));
            Console.WriteLine(x++ + y * 5);
            Console.WriteLine(y - x++ * z);
        }
    }
}