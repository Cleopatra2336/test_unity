﻿using System;

namespace Task3
{
    internal class Program
    {
        public static void Main(string[] args)

        {
            int radius_circle = 11;
            double area_circle = Math.PI * Math.Pow(radius_circle, 2);
            Console.WriteLine(area_circle);
        }
    }
}