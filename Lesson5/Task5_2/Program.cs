﻿using System;

namespace task5_2
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int x = 5, y = 10, z = 15;
            Console.WriteLine(x + y >> x++ * z);
            Console.WriteLine(++x & y * 5);
            Console.WriteLine(y / x + 5 | z);
            Console.WriteLine(x++ & y * 5);
            Console.WriteLine(y << x++ ^ z);
        }
    }
}