﻿using System;

namespace Task5_3
{
    internal class Program
    {
        public static void Main(string[] args)
        {
            int salary = 1000;
            int year_exp = Convert.ToInt32(Console.ReadLine());
            if ((year_exp > 0) && (year_exp < 5))
            {
                Console.WriteLine(salary + (salary * 0.1));
            }
            else if ((year_exp >= 5) && (year_exp < 10))
            {
                Console.WriteLine(salary + (salary * 0.15));
            }
            else if ((year_exp >= 10) && (year_exp < 15))
            {
                Console.WriteLine(salary + (salary * 0.25));
            }
            else if ((year_exp >= 15) && (year_exp < 20))
            {
                Console.WriteLine(salary + (salary * 0.35));
            }
            else if ((year_exp >= 20) && (year_exp < 25))
            {
                Console.WriteLine(salary + (salary * 0.45));
            }
            else if (year_exp >= 25)
            {
                Console.WriteLine(salary + (salary * 0.5));
            }
        }
    }
}