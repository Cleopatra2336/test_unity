﻿using System;

namespace task5_1
{
    internal class Program
    {
        public static void Main()
        { int num = 13;
            if ((num & 1) == 1) // or (num%2=1)
                Console.WriteLine("odd");
            else
                Console.WriteLine("even");
        }
    }
}